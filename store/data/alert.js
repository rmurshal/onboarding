export const state = () => ({
	alertsData: [],
	buttonsData: []
})

export const mutations = {
	setAlertsData(state, payload) {
		state.alertsData = payload
	},

	setButtonsData(state, payload) {
		state.buttonsData = payload
	}
}
