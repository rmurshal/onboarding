export default async function ({ $getDataService, store }) {
	const { gridCardData } = store.state.data.card

	if (gridCardData.length < 1) {
		const { data } = await $getDataService('/json/card.json')
		store.commit('data/card/setGridCardData', data)
	}
}
