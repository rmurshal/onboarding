export default async function ({ $getDataService, store }) {
	const { headingTags, headingStyleTags } = store.state.data.headings

	if (headingTags.length < 1 || headingStyleTags.length < 1) {
		const {
			data: { headingTags, headingStyleTags }
		} = await $getDataService('/json/headings.json')

		store.commit('data/headings/setHeadingTags', headingTags)
		store.commit('data/headings/setHeadingStyleTags', headingStyleTags)
	}
}
