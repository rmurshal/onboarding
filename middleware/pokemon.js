export default async function ({ $getDataService, store }) {
	const { pokemons } = store.state.pokemon

	if (pokemons.length < 1) {
		const pokeApiUrl = process.env.POKE_API_URL
		const requestUrl = `${pokeApiUrl}/pokemon?limit=20`
		const {
			data: { results }
		} = await $getDataService(requestUrl)
		const pokemonList = await Promise.all(
			results.map(async result => {
				return await $getDataService(result.url)
			})
		)
		const formatPokemonList = pokemonList.map(item => item.data)
		store.commit('pokemon/setPokemons', formatPokemonList)
	}
}
