export const state = () => ({
	coreData: [],
	sizingData: [],
	iconOnlyData: [],
	iconTextData: []
})

export const mutations = {
	setCoreData(state, payload) {
		state.coreData = payload
	},

	setSizingData(state, payload) {
		state.sizingData = payload
	},

	setIconOnlyData(state, payload) {
		state.iconOnlyData = payload
	},

	setIconTextData(state, payload) {
		state.iconTextData = payload
	}
}
