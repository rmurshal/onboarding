export const state = () => ({
	nodeJsLinks: [],
	gitLinks: [],
	xamppLink: {},
	generatorBazeLink: {}
})

export const mutations = {
	setNodeJsLinks(state, payload) {
		state.nodeJsLinks = payload
	},

	setGitLinks(state, payload) {
		state.gitLinks = payload
	},

	setXamppLink(state, payload) {
		state.xamppLink = payload
	},

	setGeneratorBazeLink(state, payload) {
		state.generatorBazeLink = payload
	}
}
