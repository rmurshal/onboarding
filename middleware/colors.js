export default async function ({ $getDataService, store }) {
	const { textColorTags, backgroundColorTags, blackTonesTags, linkColorList } =
		store.state.data.colors

	if (
		textColorTags.length < 1 ||
		backgroundColorTags.length < 1 ||
		blackTonesTags.length < 1 ||
		linkColorList.length < 1
	) {
		const {
			data: {
				textColorTags,
				backgroundColorTags,
				blackTonesTags,
				linkColorList
			}
		} = await $getDataService('/json/colors.json')

		store.commit('data/colors/setTextColorTags', textColorTags)
		store.commit('data/colors/setBackgroundColorTags', backgroundColorTags)
		store.commit('data/colors/setBlackTonesTags', blackTonesTags)
		store.commit('data/colors/setLinkColorList', linkColorList)
	}
}
