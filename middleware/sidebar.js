export default async function ({ $getDataService, store }) {
	const { links } = store.state.data.sidebar

	if (links.length < 1) {
		const { data } = await $getDataService('/json/sidebar.json')

		store.commit('data/sidebar/setLinks', data)
	}
}
