export const state = () => ({
	pokemons: [],
	searchQuery: ''
})

export const mutations = {
	setPokemons(state, payload) {
		state.pokemons = payload
	},

	setSearchQuery(state, payload) {
		state.searchQuery = payload
	}
}
