export default async function ({ $getDataService, store }) {
	const { nodeJsLinks, gitLinks, xamppLink, generatorBazeLink } =
		store.state.data['get-started']

	if (
		nodeJsLinks.length < 1 ||
		gitLinks.length < 1 ||
		JSON.stringify(xamppLink) === '{}' ||
		JSON.stringify(generatorBazeLink) === '{}'
	) {
		const {
			data: { nodeJsLinks, gitLinks, xamppLink, generatorBazeLink }
		} = await $getDataService('/json/get-started.json')

		store.commit('data/get-started/setNodeJsLinks', nodeJsLinks)
		store.commit('data/get-started/setGitLinks', gitLinks)
		store.commit('data/get-started/setXamppLink', xamppLink)
		store.commit('data/get-started/setGeneratorBazeLink', generatorBazeLink)
	}
}
