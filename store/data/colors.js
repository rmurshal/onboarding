export const state = () => ({
	textColorTags: [],
	backgroundColorTags: [],
	blackTonesTags: [],
	linkColorList: []
})

export const mutations = {
	setTextColorTags(state, payload) {
		state.textColorTags = payload
	},

	setBackgroundColorTags(state, payload) {
		state.backgroundColorTags = payload
	},

	setBlackTonesTags(state, payload) {
		state.blackTonesTags = payload
	},

	setLinkColorList(state, payload) {
		state.linkColorList = payload
	}
}
