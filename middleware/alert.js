export default async function ({ $getDataService, store }) {
	const { alertsData, buttonsData } = store.state.data.alert

	if (alertsData.length < 1 || buttonsData.length < 1) {
		const {
			data: { alertsData, buttonsData }
		} = await $getDataService('/json/alert.json')

		store.commit('data/alert/setAlertsData', alertsData)
		store.commit('data/alert/setButtonsData', buttonsData)
	}
}
