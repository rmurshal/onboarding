export const state = () => ({
	links: []
})

export const mutations = {
	setLinks(state, payload) {
		state.links = payload
	}
}
