export default ({ $axios }, inject) => {
	inject('getDataService', path => {
		try {
			return $axios.get(path)
		} catch (error) {
			const errData = error.response?.data
			const errMessage =
				error.response.data?.message || error.response.statusText
			if (errData.status === 404 || error.response.status === 404)
				this.$nuxt.error(this.$catch404)
			else {
				this.$nuxt.error(this.$catch500)
				const consoleError = new Error(errMessage)
				consoleError.code = errData?.status || error.response.status
				throw consoleError
			}
		}
	})
}
