export const state = () => ({
	gridCardData: []
})

export const mutations = {
	setGridCardData(state, payload) {
		state.gridCardData = payload
	}
}
