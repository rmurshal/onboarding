export const state = () => ({
	headingTags: [],
	headingStyleTags: []
})

export const mutations = {
	setHeadingTags(state, payload) {
		state.headingTags = payload
	},

	setHeadingStyleTags(state, payload) {
		state.headingStyleTags = payload
	}
}
