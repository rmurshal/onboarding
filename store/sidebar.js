export const state = () => ({
	isSidebarOpen: false
})

export const mutations = {
	changeSidebarStatus(state, status) {
		state.isSidebarOpen = status
	}
}
