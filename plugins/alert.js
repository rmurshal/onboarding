setInterval(runTriggerAlert, 100)

function runTriggerAlert() {
	const jsTriggerAlert = document.body.querySelectorAll('.js-trigger-alert')

	jsTriggerAlert.forEach(item => {
		item.addEventListener('click', () => {
			handleClick(item)
		})
	})
}

function handleClick(item) {
	// get target
	const target = item.getAttribute('data-alert')
	const targetElement = document.body.querySelector(target)

	// show alert
	changeElementDisplay(targetElement, 'block')

	// close via target background
	const targetElementContent = targetElement.querySelector(
		'.alert--popup-content'
	)
	targetElementContent.addEventListener('click', event => {
		event.stopPropagation()
	})
	targetElement.addEventListener('click', () => {
		changeElementDisplay(targetElement, 'none')
	})

	// close via target button
	const targetClose = targetElement.querySelector('.js-alert-close')
	targetClose.addEventListener('click', () => {
		changeElementDisplay(targetElement, 'none')
	})
}

function changeElementDisplay(targetElement, display) {
	targetElement.style.display = display
}
