export default async function ({ $getDataService, store }) {
	const { coreData, sizingData, iconOnlyData, iconTextData } =
		store.state.data.buttons

	if (
		coreData.length < 1 ||
		sizingData.length < 1 ||
		iconOnlyData.length < 1 ||
		iconTextData.length < 1
	) {
		const {
			data: { coreData, sizingData, iconOnlyData, iconTextData }
		} = await $getDataService('/json/buttons.json')

		store.commit('data/buttons/setCoreData', coreData)
		store.commit('data/buttons/setSizingData', sizingData)
		store.commit('data/buttons/setIconOnlyData', iconOnlyData)
		store.commit('data/buttons/setIconTextData', iconTextData)
	}
}
